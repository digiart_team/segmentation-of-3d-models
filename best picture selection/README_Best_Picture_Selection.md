The function selectBestPicture returns the three most appropriate views for each ROI for image segmentation. The outputs of the function are the image names of the most
suitable views (three strings). 

The input arguments are the name of a .txt file containing  the exterior orientation of each view (Xo, Yo, Zo in meters, and roll, pitch, yaw angles in degrees), 
the X, Y, Z coordidantes of the barycenter of a 3D ROI in meters, the focal length (c) and the coordinates of the principal point (xo,yo) in mm. 
Along with the name of a .txt file containing the names of the images and the dimentions of the images in mm (along X and Y direction). 

It is important to notice that the sequence of the images in the two .txt files (the one recording the exterior orientation of each view and the one recording the names 
of the images) has to be the same. 

There is an example entitled 'example_bestPicture.m', demonstrating the usage of the function.  

MATLAB has to be installed.
