#
include < pcl / ModelCoefficients.h > #include < pcl / point_types.h > #include < pcl / io / pcd_io.h > #include < pcl / filters / extract_indices.h > #include < pcl / filters / voxel_grid.h > #include < pcl / features / normal_3d.h > #include < pcl / kdtree / kdtree.h > #include < pcl / sample_consensus / method_types.h > #include < pcl / sample_consensus / model_types.h > #include < pcl / segmentation / sac_segmentation.h > #include < pcl / segmentation / extract_clusters.h > #include < converter.h > #include < iostream > #include < string > #include < math.h > #include < pcl\ visualization\ cloud_viewer.h >

  //check if the source file is .obj or .pcd
  bool check_ext(const std::string & fn,
    const std::string & ext) {
    return fn.substr(fn.find_last_of(".") + 1) == ext;
  }

//replace the extention of the file
std::string replace_ext(const std::string & fn,
  const std::string & ext) {
  return fn.substr(0, fn.find_last_of(".")) + "." + ext;
}

int main(int argc, char * * argv) {

  //the name of the file storing the barycenter of each segmented region of interest
  string filename = "barycenters.txt";
  ofstream myfile;
  myfile.open(filename, std::ios_base::app);

  //load the .obj file
  std::string srcfilename = "D:\\skeleton107 bench.obj";

  std::string dstfilename;

  //convert it to .pcd (necessary input for pcl function)
  if (check_ext(srcfilename, "obj")) {
    if (dstfilename.empty())
      dstfilename = replace_ext(srcfilename, "pcd");
    std::cout << "Converting " << srcfilename << " -> " << dstfilename << std::endl;
    Converter::obj2pcd(srcfilename, dstfilename);
    std::cout << "Done." << std::endl;
  } else {
    std::cerr << "Error: The file extension must be .obj or .pcd" << std::endl;
    return 1;
  }

  // Read in the cloud data
  pcl::PCDReader reader;
  pcl::PointCloud < pcl::PointXYZ > ::Ptr cloud(new pcl::PointCloud < pcl::PointXYZ > ), cloud_f(new pcl::PointCloud < pcl::PointXYZ > );
  reader.read(dstfilename, * cloud);

  //Print the number of points that constitute the point cloud
  std::cout << "PointCloud before filtering has: " << cloud - > points.size() << " data points." << std::endl; //*

  // Create the filtering object: downsample the dataset using a leaf size of 1cm (in order tor reduce the number of points in the point cloud)
  //change 0.01 if you want to change the leaf size
  pcl::VoxelGrid < pcl::PointXYZ > vg;
  pcl::PointCloud < pcl::PointXYZ > ::Ptr cloud_filtered(new pcl::PointCloud < pcl::PointXYZ > );
  vg.setInputCloud(cloud);
  vg.setLeafSize(0.01 f, 0.01 f, 0.01 f);
  vg.filter( * cloud_filtered);
  std::cout << "PointCloud after filtering has: " << cloud_filtered - > points.size() << " data points." << std::endl; //*

  // Create the segmentation object for the planar model and set all the parameters
  pcl::SACSegmentation < pcl::PointXYZ > seg;
  pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
  pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
  pcl::PointCloud < pcl::PointXYZ > ::Ptr cloud_plane(new pcl::PointCloud < pcl::PointXYZ > ());
  pcl::PCDWriter writer;
  seg.setOptimizeCoefficients(true);
  seg.setModelType(pcl::SACMODEL_PLANE);
  seg.setMethodType(pcl::SAC_RANSAC);

  //change this variable for the max number of iterations performed in order to identify a plane
  int maxIterations = 100;
  seg.setMaxIterations(maxIterations);

  //change this variable for the distance between a point and the plane in order to be considered as an inlier (here 2cm)
  float distanceThreshold = 0.02;
  seg.setDistanceThreshold(distanceThreshold);

  int i = 0, nr_points = (int) cloud_filtered - > points.size();
  int counter = 0;
  double meanX;
  double meanY;
  double meanZ;
  while (cloud_filtered - > points.size() > 0.3 * nr_points) {

    meanX = 0;
    meanY = 0;
    meanZ = 0;

    // Segment the largest planar component from the remaining cloud
    seg.setInputCloud(cloud_filtered);
    seg.segment( * inliers, * coefficients);
    if (inliers - > indices.size() == 0) {
      std::cout << "Could not estimate a planar model for the given dataset." << std::endl;
      break;
    }

    // Extract the planar inliers from the input cloud
    pcl::ExtractIndices < pcl::PointXYZ > extract;
    extract.setInputCloud(cloud_filtered);
    extract.setIndices(inliers);
    extract.setNegative(false);
    extract.filter( * cloud_plane); // Get the points associated with the planar surface

    std::stringstream ff;
    ff << "cloud_plane_" << counter << ".pcd";
    writer.write < pcl::PointXYZ > (ff.str(), * cloud_plane, false); //*

    string sourcefilename1 = ff.str();
    string destinationfilename1;

    //print the number of points of the detected plane component
    std::cout << "PointCloud representing the planar component: " << cloud_plane - > points.size() << " data points." << std::endl;

    //compute the barycenter of each component
    for (int j = 0; j < inliers - > indices.size(); j++) {

      meanX = meanX + cloud_filtered - > points[inliers - > indices[j]].x;
      meanY = meanY + cloud_filtered - > points[inliers - > indices[j]].y;
      meanZ = meanZ + cloud_filtered - > points[inliers - > indices[j]].z;

    }

    meanX = meanX / inliers - > indices.size();
    meanY = meanY / inliers - > indices.size();
    meanZ = meanZ / inliers - > indices.size();

    //write the X Y Z coordinates of the barycenter to a file
    myfile << meanX << " " << meanY << " " << meanZ << " " << endl;

    //convert from .pcd to .obj
    if (check_ext(sourcefilename1, "pcd")) {
      if (destinationfilename1.empty())
        destinationfilename1 = replace_ext(sourcefilename1, "obj");
      std::cout << "Converting " << sourcefilename1 << " -> " << destinationfilename1 << std::endl;
      Converter::pcd2obj(sourcefilename1, destinationfilename1);
      std::cout << "Done." << std::endl;
    } else {
      std::cerr << "Error: The file extension must be .obj or .pcd" << std::endl;
      //return 1;
    }

    // Remove the planar inliers, extract the rest
    extract.setNegative(true);
    extract.filter( * cloud_f); * cloud_filtered = * cloud_f;

    counter++;
  }

  // Creating the KdTree object for the search method of the extraction
  pcl::search::KdTree < pcl::PointXYZ > ::Ptr tree(new pcl::search::KdTree < pcl::PointXYZ > );
  tree - > setInputCloud(cloud_filtered);

  //set cluster tolerance, here set 2cm
  float clusterTolerance = 0.02;

  //set min number of points that constitute a cluster
  int minClusterSize = 1000;

  //set max number of points that constitute a cluster
  int maxClusterSize = 25000;

  std::vector < pcl::PointIndices > cluster_indices;
  pcl::EuclideanClusterExtraction < pcl::PointXYZ > ec;
  ec.setClusterTolerance(clusterTolerance);
  ec.setMinClusterSize(minClusterSize);
  ec.setMaxClusterSize(maxClusterSize);
  ec.setSearchMethod(tree);
  ec.setInputCloud(cloud_filtered);
  ec.extract(cluster_indices);

  int j = 0;
  for (std::vector < pcl::PointIndices > ::const_iterator it = cluster_indices.begin(); it != cluster_indices.end(); ++it) {

    //compute the barycenter of each cluster
    meanX = 0;
    meanY = 0;
    meanZ = 0;
    int f = 0;
    pcl::PointCloud < pcl::PointXYZ > ::Ptr cloud_cluster(new pcl::PointCloud < pcl::PointXYZ > );
    for (std::vector < int > ::const_iterator pit = it - > indices.begin(); pit != it - > indices.end(); ++pit) {
      cloud_cluster - > points.push_back(cloud_filtered - > points[ * pit]); //*

      meanX = meanX + cloud_filtered - > points[ * pit].x;
      meanY = meanY + cloud_filtered - > points[ * pit].y;
      meanZ = meanZ + cloud_filtered - > points[ * pit].z;
      f++;
    }

    meanX = meanX / f;
    meanY = meanY / f;
    meanZ = meanZ / f;

    //write the X Y Z barycenter coordinates to a file
    myfile << meanX << " " << meanY << " " << meanZ << " " << endl;

    cloud_cluster - > width = cloud_cluster - > points.size();
    cloud_cluster - > height = 1;
    cloud_cluster - > is_dense = true;

    //print the number of points for each cluster
    std::cout << "PointCloud representing the Cluster: " << cloud_cluster - > points.size() << " data points." << std::endl;
    std::stringstream ss;
    ss << "cloud_cluster_" << j << ".pcd";

    //save the .pcd files that correspond to the different segments
    writer.write < pcl::PointXYZ > (ss.str(), * cloud_cluster, false); //*

    string sourcefilename = ss.str();

    cout << "sourcefilename" << sourcefilename;

    string destinationfilename;

    //convert the .pcd files into .obj files and save the different .obj files
    if (check_ext(sourcefilename, "pcd")) {
      if (destinationfilename.empty())
        destinationfilename = replace_ext(sourcefilename, "obj");
      std::cout << "Converting " << sourcefilename << " -> " << destinationfilename << std::endl;
      Converter::pcd2obj(sourcefilename, destinationfilename);
      std::cout << "Done." << std::endl;
    } else {
      std::cerr << "Error: The file extension must be .obj or .pcd" << std::endl;
      //return 1;
    }

    j++;
  }

  return (0);
}