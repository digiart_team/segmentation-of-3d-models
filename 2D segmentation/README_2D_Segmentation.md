In order to run the example_segmentation, you have to download the random_walker package from http://cns.bu.edu/~lgrady/software.html. Installation of the Graph
Analysis toolbox is also required, which shall be downloaded from http://eslab.bu.edu/software/graphanalysis/.

The function performs image segmentaion based on seeds. The theory of the algorithm is explained in the paper:
Leo Grady and Gareth Funka-Lea, "Multi-Label Image Segmentation for Medical Applications Based on Graph-Theoretic Electrical Potentials", in Proceedings of the 8th ECCV04,
 Workshop on Computer Vision Approaches to Medical Image Analysis and Mathematical Methods in Biomedical Image Analysis, p. 230-245, May 15th, 2004, Prague, Czech Republic,
 Springer-Verlag. Available at: http://cns.bu.edu/~lgrady/grady2004multilabel.pdf

MATLAB 6.5 or a later version has to be installed.

