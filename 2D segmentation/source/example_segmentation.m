%An example explaining how to use random_walker for image segmentation
clear
clc
close all

%Load the image to be segmented
img=im2double(imread('sampleImage.jpg'));

%Compute the dimensions of the image
X = size(img,1);
Y = size(img,2);

 
 %Set two seeds. The first seed is the barycenter of the
 %projected ROI, the second one a background seed
 s1x=130; s1y=150; 
 s2x=200; s2y=200; 

%call the random_walker function to segment the image
[mask,probabilities] = random_walker(img,[sub2ind([X Y],s1y,s1x), ...
    sub2ind([X Y],s2y,s2x)],[1,2]);

%Visualise the original image to be segmented
figure
imagesc(img);
axis equal
axis tight
axis off
title('Original Image to be segmented')

%Visualise the two seeds 
figure
imagesc(img);
axis equal
axis tight
axis off
hold on
plot(s1x,s1y,'g.','MarkerSize',24)
plot(s2x,s2y,'b.','MarkerSize',24)
title('Image with foreground (green) and background (blue) seeds')

%Visualise the results 
figure
imagesc(mask)
colormap('gray')
axis equal
axis tight
axis off
hold on
plot(s1x,s1y,'g.','MarkerSize',24)
plot(s2x,s2y,'b.','MarkerSize',24)
title('Final segmented binary image with seeds');


